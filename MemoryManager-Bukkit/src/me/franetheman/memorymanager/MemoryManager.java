package me.franetheman.memorymanager;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import me.franetheman.memorymanager.events.HighMemoryEvent;
import me.franetheman.memorymanager.events.MemoryCheckEvent;

public final class MemoryManager {

	private long maxMemoryMB;
	private long availableMemoryMB;

	private int usedMemoryPercentage;
	private int availableMemoryPercentage;

	public MemoryManager(JavaPlugin plugin, boolean autoRun) {
		if(autoRun)
			run(plugin);
	}

	private void run(JavaPlugin plugin){
		
		new BukkitRunnable() {
			
			@Override
			public void run() {
				gatherMemoryInformation();
			}
		}.runTaskTimer(plugin, 0, 15*20);
		
	}
	
	public void gatherMemoryInformation() {
		
		maxMemoryMB = Runtime.getRuntime().maxMemory() / 1048576;
		availableMemoryMB = Runtime.getRuntime().freeMemory() / 1048576;

		usedMemoryPercentage = (int) (availableMemoryMB / (maxMemoryMB / 100));
		availableMemoryPercentage = 100 - usedMemoryPercentage;
		
		MemoryCheckEvent checkEvent = new MemoryCheckEvent(maxMemoryMB, availableMemoryMB, usedMemoryPercentage, availableMemoryPercentage);
		Bukkit.getPluginManager().callEvent(checkEvent);
		
		if(availableMemoryPercentage <= 10){
			HighMemoryEvent highMemoryEvent = new HighMemoryEvent(maxMemoryMB, availableMemoryMB, usedMemoryPercentage, availableMemoryPercentage);
			Bukkit.getPluginManager().callEvent(highMemoryEvent);
		}
	}

	public long getMaxMemory() {
		return maxMemoryMB;
	}

	public long getAvailableMemory() {
		return availableMemoryMB;
	}

	public int getAvailableMemoryPercentage() {
		return availableMemoryPercentage;
	}

	public int getUsedMemoryPercentage() {
		return usedMemoryPercentage;
	}

}
