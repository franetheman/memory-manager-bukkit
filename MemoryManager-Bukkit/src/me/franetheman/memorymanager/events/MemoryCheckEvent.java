package me.franetheman.memorymanager.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public final class MemoryCheckEvent extends Event {

	private static final HandlerList HANDLERS = new HandlerList();

	private final long maxMemoryMB;
	private final long availableMemoryMB;

	private final int usedMemoryPercentage;
	private final int availableMemoryPercentage;

	public MemoryCheckEvent(long maxMemoryMB, long availableMemoryMB, int usedMemoryPercentage,
			int availableMemoryPercentage) {
		this.maxMemoryMB = maxMemoryMB;
		this.availableMemoryMB = availableMemoryMB;
		this.usedMemoryPercentage = usedMemoryPercentage;
		this.availableMemoryPercentage = availableMemoryPercentage;
	}

	@Override
	public HandlerList getHandlers() {
		return HANDLERS;
	}

	public static HandlerList getHandlerList() {
		return HANDLERS;
	}

	public long getMaxMemoryMB() {
		return maxMemoryMB;
	}

	public long getAvailableMemoryMB() {
		return availableMemoryMB;
	}

	public int getUsedMemoryPercentage() {
		return usedMemoryPercentage;
	}

	public int getAvailableMemoryPercentage() {
		return availableMemoryPercentage;
	}

}
